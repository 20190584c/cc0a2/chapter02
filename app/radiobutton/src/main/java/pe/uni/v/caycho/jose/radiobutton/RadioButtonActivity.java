package pe.uni.v.caycho.jose.radiobutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;

public class RadioButtonActivity extends AppCompatActivity {

    ImageView imageView;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_button);

        imageView = findViewById(R.id.image_view);
        radioButton1 = findViewById(R.id.radio_button_logo_1);
        radioButton2 = findViewById(R.id.radio_button_logo_2);
        radioButton3 = findViewById(R.id.radio_button_logo_3);
        button = findViewById(R.id.button);

        button.setOnClickListener(v -> {
            if (radioButton1.isChecked()) imageView.setImageResource(R.drawable.bitcoin);
            else if (radioButton2.isChecked()) imageView.setImageResource(R.drawable.cpp);
            else if (radioButton3.isChecked()) imageView.setImageResource(R.drawable.unilogo);
        });

    }
}