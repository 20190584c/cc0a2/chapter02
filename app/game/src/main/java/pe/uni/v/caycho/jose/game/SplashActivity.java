package pe.uni.v.caycho.jose.game;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;

    Animation imageAnimation, textAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageView = findViewById(R.id.image_view_splash);
        textView = findViewById(R.id.text_view_splash);

        imageAnimation = AnimationUtils.loadAnimation(this, R.anim.image_animation);
        textAnimation = AnimationUtils.loadAnimation(this, R.anim.text_animation);

        imageView.setAnimation(imageAnimation);
        textView.setAnimation(textAnimation);

        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent next = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(next);
                finish();
            }
        }.start();
    }
}