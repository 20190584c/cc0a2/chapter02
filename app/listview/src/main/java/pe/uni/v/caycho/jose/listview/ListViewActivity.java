package pe.uni.v.caycho.jose.listview;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ListViewActivity extends AppCompatActivity {

    ListView listView;

    String[] countries;
    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listView = findViewById(R.id.list_view);

        countries = getResources().getStringArray(R.array.countries);

        arrayAdapter = new ArrayAdapter<>(ListViewActivity.this, android.R.layout.simple_list_item_1, countries );
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            String country = parent.getItemAtPosition(position).toString();
            String msg = String.format(getResources().getString(R.string.toast_message), country);
            Toast.makeText(ListViewActivity.this, msg, Toast.LENGTH_LONG).show();
        });
    }
}