package pe.uni.v.caycho.jose.spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class SpinnerActivity extends AppCompatActivity {

    ImageView imageView;
    Spinner spinner;

    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        imageView = findViewById(R.id.image_view);
        spinner = findViewById(R.id.spinner);

        adapter = ArrayAdapter.createFromResource(this, R.array.logos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) imageView.setImageResource(R.drawable.unilogo);
                else if(position == 1) imageView.setImageResource(R.drawable.cpp);
                else if(position == 2) imageView.setImageResource(R.drawable.bitcoin);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}